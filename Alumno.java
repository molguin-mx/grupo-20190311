
/**
 * Write a description of class Alumno here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Alumno
{
    private String nombre;
    public int matricula;
    private Grupo grupo;

    /**
     * Constructor for objects of class Alumno
     */
    public Alumno(String nombre, int matricula)
    {
       this.nombre = nombre;
       this.matricula = matricula;
       this.grupo = null;
    }
    
    /**
     * Constructor for objects of class Alumno
     */
    public Alumno(String nombre, int matricula, Grupo grupo)
    {
       this.nombre = nombre;
       this.matricula = matricula;
       this.grupo = grupo;
    }

    /**
     * Incluir a un alumno en un grupo
     * 
     * @param  grupo en el que estará inscrito
     * @return     void 
     */
    public void setGrupo(Grupo grupo)
    {
       this.grupo = grupo;
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public Grupo getGrupo(){
        return this.grupo;
    }
    
    public void despliegaGrupo(){
        System.out.print("El alumno " + this.nombre);
        if(this.grupo!=null)
          System.out.println(" está inscrito en: "+this.grupo);
        else
          System.out.println(" no está inscrito en un grupo");
    }
    
    public int getMatricula(){
        return this.matricula;
    }
    
    public String toString(){
        return "Nombre: "+this.nombre+" - Matrícula: "+this.matricula;
    }
}
