
/**
 *
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Maestro
{
    private int numeroDeEmpleado;
    private String nombre;

    /**
     * Constructor for objects of class Maestro
     */
    public Maestro(int numEmp, String nombre)
    {
        this.numeroDeEmpleado = numEmp;
        this.nombre = nombre;
    }

    /**
     *  
     */
    public int getNumeroDeEmpleado(){
        return this.numeroDeEmpleado;
    }
    
    public String getNombre(){
        return this.nombre;
    }
}
