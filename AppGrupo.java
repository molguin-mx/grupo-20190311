import java.util.Scanner;
public class AppGrupo
{
    
    /**
     * Constructor for objects of class AppGrupo
     */
    public AppGrupo()
    {
    }

    
    public static void run()
    {    
        Scanner entrada = new Scanner(System.in);
        
        Curso c = new Curso(100,"POO");
        Maestro m = new Maestro(5000,"Mario");        
        Grupo g = new Grupo("240",c,m);

        Alumno a = new Alumno("José",1000,g);
        g.inscribir(a);
        g.inscribir(new Alumno("María",2000,g));
        g.inscribir(new Alumno("Guadalupe",3000,g));
        
        g.despliegaListaDeAlumnos();
        
        System.out.print("\nMatricula a buscar: ");
        int respuestaNumero = entrada.nextInt();
        Alumno encontrado = g.iteraAlumnoPorMatricula(respuestaNumero);
        if(encontrado != null){
          System.out.println("Alumno encontrado "+ encontrado);
        }else {
           System.out.println("El alumno con matrícula "+respuestaNumero+" no fue encontrado");
        }
        
    }
}
