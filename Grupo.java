import java.util.ArrayList;
import java.util.Iterator;
/**
 * Modela un grupo de clase asociado a un curso, un maestro
 * y varios alumnos.
 * @author unknown 
 * @version 0.1
 */
public class Grupo
{
   private String id;
   private Maestro maestro;
   private Curso curso;
   private ArrayList<Alumno> alumnos;
   

    /**
     * Constructor de grupos default
     */
    public Grupo(String id)
    {
       this.id = id;
       this.maestro = null;
       this.curso = null;
       this.alumnos = new ArrayList<Alumno>();
    }
    
    /**
     * Constructor de Grupo con objetos curso y maestro 
     * previamente creados
     */
    public Grupo(String id, Curso c, Maestro m)
    {
       this.id = id;
       this.maestro = m;
       this.curso = c;
       this.alumnos = new ArrayList<Alumno>();
    }
    
    /**
     * Constructor de grupos con objetos curso, maestro y alumnos
     * previamente creados
     */
    public Grupo(String id, Curso c, Maestro m, ArrayList<Alumno> a)
    {
       this.id = id;
       this.maestro = m;
       this.curso = c;
       this.alumnos = a;
    }

    /**
     * Regresa la lista de alumnos
     * @return    ArrayList de objetos tipo Alumno
     */
    public ArrayList<Alumno> getAlumnos()
    {
        return alumnos;
    }
    
    /**
     * Despliega la lista de alumnos
     * @return  void
     */
    public void despliegaListaDeAlumnos()
    {
        System.out.println("Lista de alumnos del grupo: "+this.id);
        if(alumnos.isEmpty()){
            System.out.println("No hay alumnos inscritos");
            return;
        }
        for(Alumno a: alumnos){
            System.out.println("Nombre: "+
                               a.getNombre()+
                               " - Matricula: "+
                               a.matricula);
        }
            
    }
    
    public void inscribir(Alumno a){
        alumnos.add(a);
    }
    
    public String toString(){
        String cadenaFormateada = "Grupo: "+this.id;
        return cadenaFormateada;
    }
    
    public Alumno buscaAlumnoPorMatricula(int matricula){
        for(Alumno a: this.alumnos){
            if(a.getMatricula() == matricula){
                return a;
            }
        }
        return null;
    }
    
     public Alumno iteraAlumnoPorMatricula(int matricula){
        Iterator<Alumno> iterador = alumnos.iterator();
        Alumno a;
        while(iterador.hasNext()){
            a = iterador.next();
            if(a.getMatricula() == matricula){
                return a;
            }
        }
        return null;
    }
    
    public Maestro getMaestro(){
        return this.maestro;
    }
    
    public Curso getCurso(){
        return this.curso;
    }
}
