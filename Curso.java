
/**
 * 
 */
public class Curso
{
    private int clave;
    private String nombre;

    /**
     * Constructor for objects of class Curso
     */
    public Curso(int clave, String nombre)
    {
        this.clave = clave;
        this.nombre = nombre;
    }

    /**
     *  
     */
    public int getClave(){
        return this.clave;
    }
    
    public String getNombre(){
        return this.nombre;
    }
}
